<!DOCTYPE html>
<html>
    <head>
    <head>
        <title>Crowdlending</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?PHP include("imports.php");?>
        <link rel="stylesheet" href="css/estilos.css">    
    </head>
    <body>
        <?PHP include("menu.php");?>
        <div id="baner">
            <div class="container fluid">
                <div class="row">
                    <p class="titulo col-12 col-sm-12 col-lg-12" style="font-family:Roboto Slab;font-weight: 100">Acerca de Crowdlending</p>
                    <p class="col-12 col-sm-12 col-lg-12"> ¿Y si fueras tu quien le presta dinero a una empresa?</p>
                </div>
            </div>
        </div>  
        <div class="descripcion">
            <h1>- Crowdlending -</h1>
            <div class="container fluid">
                <div class="row">
                    <p class="col-12 col-sm-12 col-lg-12">
                    Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit. <br>
                    Integer posuere erat a ante.
                    Lorem ipsum dolor sit amet,<br>
                    consectetur adipiscing elit. 
                    Integer posuere erat a ante.<br>
                    Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit. <br>
                    Integer posuere erat a ante.
                    Lorem ipsum dolor sit amet,<br>
                    consectetur adipiscing elit. 
                    Integer posuere erat a ante.<br>
                    </p>
                </div>
            </div>
        </div> 
        <div class="contenedor-imagenes-nosotros">
            <div class="container fluid contenedor-nosotros">
                <div class="row">
                    <div class="col-lg-3 col-12 col-sm-12 tarjeta justify-content-center" >
                        <div class="card">
                            <img class="card-img-top" src="imagenes/vision.png" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">Visión</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 offset-lg-1 col-12 col-sm-12 tarjeta justify-content-center">
                        <div class="card">
                            <img class="card-img-top" id="mision" src="imagenes/mision.png" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">Misión</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 offset-lg-1 col-12 col-sm-12 tarjeta justify-content-center">
                        <div class="card">
                            <img class="card-img-top" src="imagenes/objetivos.png" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">Objetivos</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <?PHP 
            include("ingresar.php");
            include("registro.php");
            include("footer.php");
        ?> 
    </body>
</html>