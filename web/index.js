var procesos = {
    init: function () {
        procesos.navegador();
        procesos.validaciones();
        procesos.editar();
        procesos.eliminar();
    },
    navegador: function () {
        $("#desplegar_menu").off("click").on("click", function () {
            $("#menu").slideToggle(500);
        });
        $(window).resize(function(){
            if($(window).width()>=768){
                $("#menu").css({display:"none"});
            }
        });
    },
    validaciones: function(){
        var expresion_numeros=/\D/;  //detecta todo menos numeros
        var expresion_nombres_apellidos=/^[A-Za-z]{1,}\s{0,1}[A-Za-z]{0,}$/; //detecta todo menos texto
        var expresion_correo=/^[A-Za-z0-9]{1,}[@][A-Za-z]{1,}[.][A-Za-z]{1,}$/; //detecta todo menos texto
        imagen_bien="<img src='bien.png' style='width:15px;height:15px;'>";
        imagen_mal="<img src='mal.png' style='width:15px;height:15px;'>";
        function identificacion_telefono(nombre){
            var temp=$("#"+nombre).val();
            if(!temp || temp.length>30 || expresion_numeros.test(temp)){
                $("#"+nombre).parent().find(".imagen_validar").html(imagen_mal);
                return false;
            }
            $("#"+nombre).parent().find(".imagen_validar").html(imagen_bien);
            return true;
        };
        function nombre_apellidos(nombre){
            var temp=$("#"+nombre).val();
            if(!temp || temp.length>30 || !expresion_nombres_apellidos.test(temp)){
                $("#"+nombre).parent().find(".imagen_validar").html(imagen_mal);
                return false;
            }
            $("#"+nombre).parent().find(".imagen_validar").html(imagen_bien);
            return true;
        };
        function correo(){
            var temp=$("#correo").val();
            if(!temp || temp.length>30 || !expresion_correo.test(temp)){
                $("#correo").parent().find(".imagen_validar").html(imagen_mal);
                return false;
            }
            $("#correo").parent().find(".imagen_validar").html(imagen_bien);
            return true;
        };
        function contrasena(){
            var temp=$("#contrasena").val();
            if(!temp || temp.length>30){
                $("#contrasena").parent().find(".imagen_validar").html(imagen_mal);
                return false;
            }
            $("#contrasena").parent().find(".imagen_validar").html(imagen_bien);
            return true;
        };
        function cuenta(){
            var temp=$("#cuenta").val();
            if(!temp){
                $("#cuenta").parent().find(".imagen_validar").html(imagen_bien);
                return true;
            }else{
                if(temp.length>30 || expresion_numeros.test(temp)){
                    $("#cuenta").parent().find(".imagen_validar").html(imagen_mal);
                    return false;
                }
            }
            $("#cuenta").parent().find(".imagen_validar").html(imagen_bien);
            return true;
        };
        
        $("#identificacion").on("keyup",function(){
            identificacion_telefono("identificacion");
        });
        $("#telefono").on("keyup",function(){
            identificacion_telefono("telefono");
        });
        $("#cuenta").on("keyup",function(){
            cuenta();
        });
        $("#nombre").on("keyup",function(){
            nombre_apellidos("nombre");
        });
        $("#apellidos").on("keyup",function(){
            nombre_apellidos("apellidos");
        });
        $("#correo").on("keyup",function(){
            correo();
        });
        $("#contrasena").on("keyup",function(){
            contrasena();
        });
        $("#formulario_registrarse").on("submit",function(){
            if(!identificacion_telefono("identificacion") || !identificacion_telefono("telefono") || !cuenta() || !nombre_apellidos("nombre") || !nombre_apellidos("apellidos") || !correo() || !contrasena()){
                $("#alerta_registro").css({display:"block"});
                return false; // ESTO PARA QUE NO RECARGUE LA PAGINA DEL FORM
            }else{
                //SI LAS VALIDACIONES ESTAN CORRECTAS
                peticion_registrarse();
            }
        });   
    },
    peticion_registrarse: function(){

    },
    peticion_ingreso: function(){

    },
    editar:function(){
        $(".editar").on("click",function(){
            var variable=$(this);
            if($(this).parent().find("input")[0].disabled==true){
                if($(this).parent().find("input")[0].attributes.id.nodeValue=="contrasena"){
                    $(this).parent().find("input")[0].disabled=false;
                    $(this).parent().find("input")[1].disabled=false;
                    $(this).css({"background-color":"green"});
                }else{
                    $(this).parent().find("input")[0].disabled=false;
                    $(this).css({"background-color":"green"});
                }
                    
            }else{
                var valor = $(this).parent().find("input")[0].value;
                var nombre = $(this).parent().find("input")[0].attributes.id.nodeValue;
                if(nombre == "contrasena"){
                    var valor2 = $(this).parent().find("input")[1].value;
                    parametros = {
                        nombre_columna: nombre,
                        valor_columna: valor,
                        valor_nuevo: valor2,
                    };
                }else{
                    parametros = {
                        nombre_columna: nombre,
                        valor_columna: valor,
                    };
                }
                $.ajax({
                    "data": parametros,
                    "url": "modificar.php",
                    "method": "POST"
                }).done(function(respuesta){
                    if (respuesta.estado === "ok") {
                        alert(respuesta.valor);
                        variable.parent().find("input")[0].disabled=true;
                        variable.css({"background-color":"#007bff"});
                        
                    }else{
                        alert(respuesta.valor);
                    }
                });
            }
        });
    },
    eliminar:function(){
        $(".eliminar").on("click",function(){
            var variable=$(this);
            if($(this).parent().find("input")[0].disabled==true){
                $(this).parent().find("input")[0].disabled=false;
                $(this).css({"background-color":"green"});
            }else{
                var parametros = {
                    valor_columna:  $(this).parent().find("input")[0].value,
                    bandera: "mostrar"
                };
                $.ajax({
                    "data": parametros,
                    "url": "eliminar.php",
                    "method": "POST"
                }).done(function(respuesta){
                    if (respuesta.estado === "ok") {
                        $("#datos").removeClass("invisible");
                        $("#confirmar").removeClass("invisible");
                        $('#datos').append(respuesta.nombre+" "+respuesta.apellidos+"<br>"+"Identificacion:"+" "+respuesta.identificacion+"<br>"+"Telefono:"+" "+respuesta.telefono+"<br>"+"Correo:"+" "+respuesta.correo+"<br>"+"Cuenta:"+" "+respuesta.cuenta+"<br>"+"Ciudad:"+" "+respuesta.ciudad+"<br>");
                        variable.parent().find("input")[0].disabled=true;
                        variable.css({"background-color":"#007bff"});
                        
                    }else{
                        alert(respuesta.valor);
                    }
                });
            }
        });
        $(".eliminar_fila").on("click",function(){
            var variable=$(this);
            var parametros = {
                valor_columna:  $(this).parent().parent().find("input")[0].value,
                bandera: "eliminar"
            };
            $.ajax({
                "data": parametros,
                "url": "eliminar.php",
                "method": "POST"
            }).done(function(respuesta){
                if (respuesta.estado === "ok") {
                    alert(respuesta.valor);

                    $("#datos").addClass("invisible");
                    $("#confirmar").addClass("invisible");
                    $('#datos').html("");
                    variable.parent().parent().find("input")[0].value="";
                    variable.parent().parent().find("input")[0].disabled=true;
                }else{
                    alert(respuesta.valor);
                }
            });
        
        });
    }
};

$(document).ready(function () {
    procesos.init();    
});
