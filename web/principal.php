<?PHP  
    session_start();
    if(isset($_SESSION["roll"])){
        if($_SESSION["roll"] != "cliente"){
            header("location: index.php");
        }
    }else{
        header("location: index.php");
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php
        include("imports.php");
    ?>
    <link rel="stylesheet" href="css/estilos.css">  
    <title>Inicio</title>
</head>
<body>
    <?PHP
        include("menu-perfil.php");
    ?>
    <div id="baner" style="padding-top: 25px;">
        <p class="titulo">Bienvenido <?PHP echo $_SESSION['nombre'];?></p>
        <p>Invierte en las ideas que mas te convengan, para eso es Crowlending</p>
        <a href="#" class="btn btn-info">Como invertir</a>
        <div class="row justify-content-center" style="margin-top: 20px;">
            <div class="col-lg-6 col-sm-6 col-12">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Busca cualquier cosa">
                    <div class="input-group-append">
                    <button class="btn btn-secondary" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                    </div>
                </div>
            </div>
        </div>     
    </div>
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-12 categorias" style="margin-bottom: 30px;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-12">
                        <h1>Categorias</h1>
                </div>
                <div class="col-lg-4 col-sm-6 col-6">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-inline-block">
                            Educación
                            <label class="switch ">
                                <input type="checkbox" class="success">
                                <span class="slider round"></span>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-sm-6 col-6">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-inline-block">
                            Infraestructura
                            <label class="switch ">
                                <input type="checkbox" class="success">
                                <span class="slider round"></span>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-sm-6 col-6">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-inline-block">
                            Creatividad
                            <label class="switch ">
                                <input type="checkbox" class="success">
                                <span class="slider round"></span>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-sm-6 col-6">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-inline-block">
                            Tecnología                            
                            <label class="switch ">
                                <input type="checkbox" class="success">
                                <span class="slider round"></span>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-sm-6 col-6">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-inline-block">
                            Industria
                            <label class="switch ">
                                <input type="checkbox" class="success">
                                <span class="slider round"></span>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-sm-6 col-6">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-inline-block">
                            Otros
                            <label class="switch">
                                <input type="checkbox" class="success">
                                <span class="slider round"></span>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-sm-12 col-12 contenedor-de-ideas">
            <div class="row ideas-principal">
            <div class="col-lg-4 col-12 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/cantar.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/hacker.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/boda.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/carro.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/ciudad.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/carro.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-sm-12 col-12">
            <div class="paginas">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <?PHP include("footer.php");?>
</body>