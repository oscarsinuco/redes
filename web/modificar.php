<?PHP
    session_start();
    include("conexion.php");
    $conexion = conectarse();
    extract($_POST);
    $id = $_SESSION['identificacion'];
    if($valor_nuevo != "" AND $valor_nuevo != NULL){
        $conexion_ldap = ldap_connect("localhost");
        ldap_set_option($conexion_ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
        $dn = "cn='$id',cn=cliente,ou=grupos,dc=crowdlending-uis-redes,dc=com";
        $autentificacion = ldap_bind($conexion_ldap, $dn, $valor_columna);
        //$consulta = "SELECT contrasena FROM usuario where identificacion = '$id'";
        //$resultado_contrasena = mysqli_query($conexion,$consulta);
        //$row = mysqli_fetch_assoc($resultado_contrasena);
        if($conexion_ldap){
            if($autentificacion){
                $entrada["userPassword"] = $valor_nuevo;
                $resultado_ldap = ldap_mod_replace($conexion_ldap, $dn, $entrada);
                if($resultado_ldap){
                    $resultado = true;
                }else{
                    $resultado = false;
                }
            }else{
                $resultado = false;
            }
        }else{
            $resultado = false;
        }
        ldap_close($conexion_ldap);
    }else{
        if($nombre_columna != "contrasena"){
            $consulta = "UPDATE usuario SET $nombre_columna='$valor_columna' WHERE identificacion = '$id'";
            $resultado = mysqli_query($conexion,$consulta);
        }else{
            $resultado = false;
        }
    }
    header('Content-Type: application/json');
    if($resultado){
        $datos = array(
            'estado' => 'ok',
            'valor' => "Éxito en la modificación"
        );
    }else{
        $datos = array(
            'estado' => 'bad',
            'valor' => "Falló la modificación"
        );
    }
    mysqli_close($conexion);
    echo json_encode($datos, JSON_FORCE_OBJECT);
?>