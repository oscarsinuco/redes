<?PHP  
    session_start();
    if(isset($_SESSION["roll"])){
        if($_SESSION["roll"] != "administrador"){
            header("location: index.php");
        }
    }else{
        header("location: index.php");
    }
?>
<?PHP
    include("conexion.php");
    $conexion = conectarse();
    extract($_POST);
    if($_POST == NULL){
        header("location: admin.php");
    }
    if($bandera=="mostrar"){
        if($valor_columna != NULL AND $valor_columna != ""){
            $consulta = "SELECT * FROM usuario WHERE identificacion='$valor_columna'";
            $resultado = mysqli_query($conexion,$consulta);
            $row = mysqli_fetch_assoc($resultado);
            
        }else{
            $resultado = false;
        }
        header('Content-Type: application/json');
        if($resultado AND $resultado->num_rows){
            $datos = array(
                'estado' => 'ok',
                'identificacion'  => $row['identificacion'],
                'telefono'  => $row['telefono'],
                'nombre'  => $row['nombre'],
                'apellidos'  => $row['apellidos'],
                'correo'  => $row['correo'],
                'cuenta'  => $row['cuenta'],
                'ciudad'  => $row['ciudad']
            );
        }else{
            $datos = array(
                'estado' => 'bad',
                'valor' => "El usuario a eliminar no existe"
            );
        }
        mysqli_close($conexion);
        echo json_encode($datos, JSON_FORCE_OBJECT);
    }else{
        if($valor_columna != NULL AND $valor_columna != ""){
            $conexion_ldap = ldap_connect("localhost");
            if($conexion_ldap){
                ldap_set_option($conexion_ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
                $r = ldap_bind($conexion_ldap, "cn=admin,dc=crowdlending-uis-redes,dc=com","123456");
                if($r){
                    $resultado_eliminar = ldap_delete($conexion_ldap,"cn='$valor_columna',cn=cliente,ou=grupos,dc=crowdlending-uis-redes,dc=com");
                    if($resultado_eliminar){
                        $consulta = "DELETE FROM usuario WHERE identificacion='$valor_columna'";
                        $resultado = mysqli_query($conexion,$consulta);
                    }else{
                        $resultado = false;
                    }
                }else{
                    $resultado = false;
                }
            }
            ldap_close($conexion_ldap);
        }else{
            $resultado = false;
        }
        header('Content-Type: application/json');
        if($resultado){
            $datos = array(
                'estado' => 'ok',
                'valor' => "Usuario eliminado exitosamente"
            );
        }else{
            $datos = array(
                'estado' => 'bad',
                'valor' => "El usuario a eliminar no existe"
            );
        }
        mysqli_close($conexion);
        echo json_encode($datos, JSON_FORCE_OBJECT);
    }


?>