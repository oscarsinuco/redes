<?PHP  
    session_start();
    if(isset($_SESSION["roll"])){
        if($_SESSION["roll"] != "administrador"){
            header("location: index.php");
        }
    }else{
        header("location: index.php");
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php
        include("imports.php");
    ?>
    <link rel="stylesheet" href="css/estilos.css">  
    <title>Nombre perfil</title>
</head>
<body>
    <?PHP
        include("menu-perfil.php");
    ?>
    <div id="baner" style="padding-top: 25px;">
        <p class="titulo">¿Quieres eliminar usuarios?</p>
        <p>En este lugar podrás eliminar usuarios</p>   
    </div> 
    <div class="card-footer text-muted">
        <p>Tu última eliminacion de usuarios fue hace 2 dias</p>
    </div>
    <div class="row edit-perfil">
        <div class="col-lg-12 col-sm-12 col-12">
            <form action="">
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-sm-6 col-12">
                        <label for="exampleInputEmail1"><small class="form-text text-muted">Identificacion a eliminar</small></label>
                        <div class="input-group input-group-sm mb-3">
                            <input type="number" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="" disabled value="<?PHP echo $row["telefono"]?>">
                            <button type="button" class="eliminar btn btn-primary">
                                <i class="fas fa-edit"></i>
                            </button>
                        </div>
                        <div class="input-group input-group-sm mb-3 invisible" id="confirmar">
                            <button type="button" class="eliminar_fila center-block btn btn-success">Confirmar eliminación</button>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-12">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-12 alert alert-primary datos invisible" id="datos" role="alert">
                                
                            </div>
                        </div>
                    </div>
                </div>                            
            </form>            
        </div>
    </div>
    <?PHP include("footer.php");?>
</body>
</html>