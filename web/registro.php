        <!--%%%%%%%%%%%%%%%%%%%%%ESTE DIV CONTIENE EL FORMULARIO DE REGISTRARSE%%%%%%%%%%%%%%%%%%%%%%%%%%%-->
        <div class="modal fade" id="modal_registrarse" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="registro modal-content" style="background-color: #dfeeee;">
                    <div class='container-fluid'>
                        <div class="row align-items-center" >
                            <div class="col-5" style="margin-left: auto;margin-right: auto; margin-bottom: 20px;">
                                <h1 style="font-weight: bold;">Registro</h1>
                            </div>
                            <div id="alerta_registro" class="col-6 offset-1" style="padding:0px;font-size:13px;display:none;" > 
                                <div class="alert alert-danger alert-dismissible fade show" style="margin:0px;padding:10px 20px 10px 10px;" role="alert" style="padding:15px 15px 15px 15px;">
                                    Por favor digite bien los campos
                                <button onclick="$('#alerta_registro').css({display:'none'});" style="padding:5px" type="button" class="close" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <form id="formulario_registrarse" action="insertarbd.php" method="POST">
                                <div class="row">
                                    <div class="form-group col-md-6 col-12 col-xs-12 col-lg-6"> 
                                        <div class="row">
                                            <label class="col-6 col-sm-4 col-md-8"  for="indentificacion">Identificación</label>
                                            <div class="imagen_validar">
                                                <!--este div es al que le voy a hacer .append una imagen dependiendo de la validacion-->
                                            </div>
                                        </div>                   
                                        <input type="number" class="form-control form-control-sm" name="identificacion" id="identificacion" placeholder="C.C u otro">
                                    </div>
                                    <div class="form-group col-md-6 col-12 col-xs-12 col-lg-6">
                                        <div class="row">
                                            <label class="col-5 col-sm-3 col-md-6"  for="telefono">Telefono</label>
                                            <div class="imagen_validar">
                                                <!--este div es al que le voy a hacer .append una imagen dependiendo de la validacion-->
                                            </div>
                                        </div>     
                                        <input type="number" class="form-control form-control-sm" id="telefono" name="telefono" placeholder="Telefono o celular">
                                    </div>
                                    <div class="form-group col-md-6 col-12 col-xs-12 col-lg-6">
                                        <div class="row">
                                            <label class="col-5 col-sm-3 col-md-6"  for="nombre">Nombres</label>
                                            <div class="col-2 col-sm-2 col-md-3 imagen_validar" style="padding: 0px;">
                                                <!--este div es al que le voy a hacer .append una imagen dependiendo de la validacion-->
                                            </div>
                                        </div> 
                                        <input type="text" class="form-control form-control-sm" id="nombre" name="nombre">
                                    </div>
                                    <div class="form-group col-md-6 col-12 col-xs-12 col-lg-6">
                                        <div class="row">
                                            <label class="col-5 col-sm-3 col-md-6"  for="apellidos">Apellidos</label>
                                            <div class="col-2 col-sm-2 col-md-3 imagen_validar" style="padding: 0px;">
                                                <!--este div es al que le voy a hacer .append una imagen dependiendo de la validacion-->
                                            </div>
                                        </div> 
                                        <input type="text" class="form-control form-control-sm" id="apellidos" name="apellido">
                                    </div>
                                    <div class="form-group col-md-6 col-12 col-xs-12 col-lg-6">
                                        <div class="row">
                                            <label class="col-4 col-sm-3 col-md-5"  for="correo">Correo</label>
                                            <div class="col-2 col-sm-2 col-md-3 imagen_validar" style="padding: 0px;">
                                                <!--este div es al que le voy a hacer .append una imagen dependiendo de la validacion-->
                                            </div>
                                        </div> 
                                        <input type="text" class="form-control form-control-sm" id="correo" name="correo" placeholder="alguien@example.com">
                                    </div>
                                    <div class="form-group col-md-6 col-12 col-xs-12 col-lg-6">
                                        <div class="row">
                                            <label class="col-6 col-sm-4 col-md-7"  for="contrasena">Contraseña</label>
                                            <div class="col-2 col-sm-2 col-md-3 imagen_validar" style="padding: 0px;">
                                                <!--este div es al que le voy a hacer .append una imagen dependiendo de la validacion-->
                                            </div>
                                        </div> 
                                        <input type="password" class="form-control form-control-sm" id="contrasena" name="contrasena" placeholder="Más de 8 caracteres">
                                    </div>
                                    <div class="form-group col-md-6 col-12 col-xs-12 col-lg-6">
                                        <div class="row">
                                            <label class="col-7 col-sm-4 col-md-8"  for="cuenta">N.cuenta(Opc)</label>
                                            <div class="col-2 col-sm-2 col-md-3 imagen_validar" style="padding: 0px;">
                                                <!--este div es al que le voy a hacer .append una imagen dependiendo de la validacion-->
                                            </div>
                                        </div> 
                                        <input type="number" class="form-control form-control-sm" id="cuenta" name="cuenta" placeholder="Número de cuenta bancaria">
                                    </div>
                                    <div class="form-group col-md-6 col-12 col-xs-12 col-lg-6">
                                        <div class="row">
                                            <label class="col-7 col-sm-3 col-md-6"  for="id_ciudad">Ciudad</label>
                                            <div class="col-2 col-sm-2 col-md-3 imagen_validar" style="padding: 0px;">
                                                <!--este div es al que le voy a hacer .append una imagen dependiendo de la validacion-->
                                            </div>
                                        </div> 
                                        <select id="id_ciudad" name="ciudad" class="form-control form-control-sm">
                                            <option value="">Seleccione...</option>
                                            <option value="bucaramanga">Bucaramanga</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 col-12 col-xs-12 col-lg-6">
                                    <br><button  id="boton-registrarse" class="btn btn-primary" style="width: 100%">Registrarse</button>
                                    </div>
                                    <div class="form-group col-md-6 col-12 col-xs-12 col-lg-6">
                                    <br><button data-dismiss="modal" class="btn btn-primary" type="button" style="width: 100%">Cancelar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>