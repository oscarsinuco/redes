<?PHP  
    session_start();
    if(isset($_SESSION["roll"])){
        if($_SESSION["roll"] == "cliente"){
            header("location: principal.php");
        }
        if($_SESSION["roll"] == "administrador"){
            header("location: admin.php");
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
    <head>
        <title>Crowdlending</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?PHP include("imports.php");?>
        <link rel="stylesheet" href="css/estilos.css">
        </head>
    <body>
        <?PHP include("menu.php");?>  
        <!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%CAROUSEL CON IMAGENES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%--> 
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100 carousel_imagen" src="imagenes/focos.jpg" >
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 carousel_imagen" src="imagenes/carousel_imagen2.jpeg">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 carousel_imagen" src="imagenes/carousel_imagen3.jpeg">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 carousel_imagen" src="imagenes/carousel_imagen4.jpeg">
                </div>
            </div>
        </div>
        <!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Texto Generico%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%--> 
        <div class="text-center informacion">
            <p id="titulo">Crowdlending</p>
            <p>Oportunidades de Crecimiento.</p>
        </div>
        <div class="contenedor-proyectos">
        <div class="alert alert-success" role="alert">
            Las ideas mas recientes y mas invertidas se presentan aquí!!
        </div>
            <div class="row">
                <div class="col-lg-4 col-12 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/cantar.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/hacker.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/boda.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/ciudad.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/carro.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/ciudad.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?PHP 
            include("ingresar.php");
            include("registro.php");
            include("footer.php");
        ?>   
    </body>
</html>
