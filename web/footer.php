<footer id="footer" class="page-footer font-small mdb-color pt-4">
            <div class="container text-center text-md-left">
                <div class="row text-center text-md-left mt-3 pb-3">

                    <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mt-3">
                        <h6 class="text-uppercase mb-4 font-weight-bold" style="color:white;font-size:20px;">CROWDLENDING</h6>
                        <p><a style="color:rgb(25,158,218);" href="principal.php">Inicio</a></p>
                        <p><a style="color:rgb(25,158,218);" onclick="$('#modal_registrarse').modal('show');" href="#">Registrarse</a></p>
                        <p><a style="color:rgb(25,158,218);" onclick="$('#modal_ingresar').modal('show');" href="#">Ingresar</a></p>
                        <p><a style="color:rgb(25,158,218);" href="#">PQRS</a></p>
                    </div>


                    <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3"style="color:white;" >
                        <h6 id="contacto" class="text-uppercase mb-4 font-weight-bold"style="font-size:20px;">CONTACTO</h6>
                        <p>
                            <i class="fas fa-home mr-3"></i> Bucaramanga, San, COL</p>
                        <p>
                            <i class="fas fa-envelope mr-3"></i> crowdlending@gmail.com</p>
                        <p>
                            <i class="fas fa-phone mr-3"></i> 3166231193</p>

                    </div>
                </div>
                <div class="row d-flex align-items-center">
                    <div class="col-md-7 col-lg-8">
                        <p class="text-center text-md-left">© 2018 Copyright:
                            <a style="color:rgb(25,158,218);" href="/crowd/index.jsp/">
                                <strong> crowdlending.com</strong>
                            </a>
                        </p>
                    </div>
                    <div class="col-md-5 col-lg-4 ml-lg-0">
                        <!-- Social buttons -->
                        <div class="text-center text-md-right">
                            <ul class="list-unstyled list-inline">
                                <li class="list-inline-item">
                                    <a class="btn-floating btn-sm rgba-white-slight mx-1"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="btn-floating btn-sm rgba-white-slight mx-1"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="btn-floating btn-sm rgba-white-slight mx-1"><i class="fab fa-google-plus-g"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="btn-floating btn-sm rgba-white-slight mx-1"><i class="fab fa-linkedin-in"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>