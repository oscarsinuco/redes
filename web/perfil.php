<?PHP  
    session_start();
    if(isset($_SESSION["roll"])){
        if($_SESSION["roll"] != "cliente"){
            header("location: index.php");
        }
    }else{
        header("location: index.php");
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php
        include("imports.php");
    ?>
    <link rel="stylesheet" href="css/estilos.css">  
    <title>Nombre perfil</title>
</head>
<body>
    <?PHP
        include("menu-perfil.php");
    ?>
    <div id="baner" style="padding-top: 25px;">
        <p class="titulo">¿Quieres ver o cambiar tus datos actuales?</p>
        <p>En este lugar podrás encontrar diferentes operaciones relacionadas con tu perfil y linea Crowdlending</p>
        <a href="#" class="btn btn-info">Modificar datos</a>        
    </div> 
    <div class="card-footer text-muted">
        <p>Tu última actualización de datos fue hace 2 dias</p>
    </div>
    <div class="row edit-perfil">
        <div class="col-lg-5 col-sm-5 col-12 imagen-perfil">
            <center>
                <div id="imagen-contenedor">
                    <img class="align-middle" src="imagenes/maduro.jpg" alt="">
                    <div id="cambiar-foto">
                        Cambiar imagen
                    </div>              
                </div>
            </center>
        </div>

        <?PHP 
            include("conexion.php");
            $conexion = conectarse();
            $atr = $_SESSION['identificacion'];
            $consulta="SELECT * FROM usuario WHERE identificacion='$atr'";
            $resultado= mysqli_query($conexion,$consulta);
            $row = mysqli_fetch_assoc($resultado);
        ?>
        <div class="col-lg-7 col-sm-7 col-12">
            <form action="">
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-sm-6 col-12">
                    <label for="exampleInputEmail1"><small class="form-text text-muted">Telefono</small></label>
                        <div class="input-group input-group-sm mb-3">
                            <input type="number" class="form-control" id="telefono" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="" disabled value="<?PHP echo $row["telefono"]?>">
                            <button type="button" class="editar btn btn-primary">
                                <i class="fas fa-edit"></i>
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1"><small class="form-text text-muted">Nombres</small></label>
                        <div class="input-group input-group-sm mb-3">
                            <input type="text" class="form-control" id="nombre" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="" disabled value="<?PHP echo $row["nombre"]?>">
                            <button type="button" class="editar btn btn-primary">
                                <i class="fas fa-edit"></i>
                                </button>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-12">
                    <label for="exampleInputEmail1"><small class="form-text text-muted">Apellidos</small></label>
                        <div class="input-group input-group-sm mb-3">
                            <input type="text" class="form-control" id="apellidos" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="" disabled value="<?PHP echo $row["apellidos"]?>">
                            <button type="button" class="editar btn btn-primary">
                                <i class="fas fa-edit"></i>
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12 col-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1"><small class="form-text text-muted">Correo</small></label>
                        <div class="input-group input-group-sm mb-3">
                            <input type="email" class="form-control" id="correo" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="" disabled value="<?PHP echo $row["correo"]?>">
                            <button type="button" class="editar btn btn-primary">
                                <i class="fas fa-edit"></i>
                            </button>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-6 col-sm-12 col-12">
                    <label for="exampleInputEmail1"><small class="form-text text-muted">Contraseña</small></label>
                        <div class="input-group input-group-sm mb-3">
                            <input type="text" class="form-control" id="contrasena" aria-label="Small" aria-describedby="inputGroup-sizing-sm" value="" placeholder="Contraseña antigua" disabled>
                            <input type="text" class="form-control" id="contrasena_nueva" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Contraseña nueva" disabled>
                            <button type="button" class="editar btn btn-primary">
                                <i class="fas fa-edit"></i>
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12 col-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1"><small class="form-text text-muted">Número de cuenta</small></label>
                        <div class="input-group input-group-sm mb-3">
                            <input type="number" class="form-control" id="cuenta" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="" disabled value="<?PHP echo $row["cuenta"]?>">
                            <button type="button" class="editar btn btn-primary">
                                <i class="fas fa-edit"></i>
                            </button>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-6 col-sm-12 col-12">
                    <label for="exampleInputEmail1"><small class="form-text text-muted">Ciudad</small></label>
                        <div class="input-group input-group-sm mb-3">
                            <input type="text" class="form-control" id="ciudad" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="" disabled value="<?PHP echo $row["ciudad"]?>">
                            <button type="button" class="editar btn btn-primary">
                                <i class="fas fa-edit"></i>
                            </button>
                        </div>
                    </div>                    
                </div>                            
            </form>            
        </div>
        <div class="container contenedor-ideas-perfil">
        <div class="alert alert-success" role="alert">
            Las ideas que has creado se presentan a continuación!
        </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-6 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/cantar.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-6 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/hacker.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-6 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/boda.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-6 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/carro.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-6 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/ciudad.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-6 col-sm-6 tarjeta justify-content-center">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="imagenes/carro.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                            </div>
                            <a href="#" class="btn btn-primary">Mas información</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?PHP include("footer.php");?>
</body>
</html>