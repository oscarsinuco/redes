
        <!--%%%%%%%%%%%%%%%%%%ESTE DIV CONTIENE EL FORMULARIO PARA INGRESAR%%%%%%%%%%%%%%%%-->
        <div class="modal fade" id="modal_ingresar" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="ingreso modal-content" style="background-color: #dfeeee;">
                    <div class='container-fluid'>
                        <div class="row align-items-center">
                            <div class="col-12 text-center" style="padding:0px;">
                                <img id="persona" style="margin-top: 30px;" src="imagenes/persona.png" width="100" height="100">
                            </div>
                        </div>
                        <div calss="row" style="margin-top:20px;">
                            <form action="login.php" method="POST">
                                <div class="formulario row">
                                    <div id="usuario_incorrecto" class="form-group col-md-12 col-12 col-xs-12 col-lg-12">

                                    </div>
                                    <div class="form-group col-md-12 col-12 col-xs-12 col-lg-12">
                                        <label for="usuario2">Usuario</label>
                                        <input type="text" class="form-control" id="usuario2" name="usuario" placeholder="Identificación, Telefono o email">
                                    </div>
                                    <div class="form-group col-md-12 col-12 col-xs-12 col-lg-12">
                                        <label for="contrasena">Contraseña</label>
                                        <div class="row no-gutters align-items-center">
                                            <div class="col">
                                                <input type="password" class="form-control" name="contrasena" id="contrasena2" >
                                            </div>
                                            <div class="col-auto">
                                                <button id="mostrar-contrasena" type="button"><img id="mostrar-contrasena-imagen" src="imagenes/mostrar.png" height="20"></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6 col-12 col-xs-12 col-lg-6">
                                        <input style="width:20px;" type="checkbox" value="" id="recordar">
                                        <label class="form-check-label texto-recordar" for="recordar">
                                            Recordar contraseña
                                        </label>
                                    </div>
                                    <div class="form-group col-md-6 col-12 col-xs-12 col-lg-6">
                                        <a href="../Crowd">He olvidado mi contraseña</a>
                                    </div>
                                    <div class="form-group col-md-6 col-12 col-xs-12 col-lg-6">
                                        <button type="submit" id="boton-iniciar-sesion" class="btn btn-primary" style="width:100%">Iniciar sesión</button>
                                    </div>
                                    <div class="form-group col-md-6 col-12 col-xs-12 col-lg-6">
                                        <button data-dismiss="modal" class="btn btn-primary" type="button" style="width:100%">Cancelar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>